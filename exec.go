package cmd

import (
	"os"
	"os/exec"
	"syscall"
)

// Exec (not to be confused with Execute) will check for the existance of the
// first argument as an executable on the system and then execute it using
// syscall.Exec(), which replaces the currently running program with the new
// one in all respects (stdin, stdout, stderr, process ID, etc).
//
// Note that although this is exceptionally faster and cleaner than calling any
// of the os/exec variations it may be less compatible with different operating
// systems.
func Exec(args ...string) error {
	path, err := exec.LookPath(args[0])
	if err != nil {
		return err
	}
	err = syscall.Exec(path, args, os.Environ())
	if err != nil {
		return err
	}
	return nil
}
